import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:blacknet_lib/blacknet.dart';
import 'package:blacknet_lib/serialize.dart';

import 'package:blacknet_lib/blacknet/utils.dart';

const testAccount = 'blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036';
const testMnemonic = 'piano maze provide discover tower scissors true leave senior aware secret film';
const testMessage = 'BLN-is-very-nice';
const testSign = 'FF6D74C0493720F59DA4F06CD6D13D4A47D04F61E6D4AFF3D001EBD59153DC6DF40DDA34F7CD63FBA76CD2C1CA3963D1CB4F17F2061FB191BA441F0BF925C40B';
const testSerialized = '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080';
const testSignature = '7cad618727b1dd3872d685c4c2eba86843e64321fe622a60a66807e5a495a7b081a04eb8eebd84b683d2cbad8b13ce6277b4e2280ec6da528d0b9f344f246a0babb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080';
const testEncrypted = '669E238CBD3977550DEAB18DEE256BCB16748CA9';
const testStr       = 'blacknet';
const testMnemonic2 = "prepare long erode easy moment dinosaur soft sound exhibit wire mesh muffin";
const testAccount2 = 'blacknet1pns3tp3wja8jmqqyfy0pvqedg9nv4zj3tkcpqmep5hr3rvw2rnkqhafpf9';

const url = 'https://blnmobiledaemon.blnscan.io';
var serialize = new Serialize(endpoint: url);
var blacknet = new Blacknet(endpoint: url);

void main() {
  // blacknet utils
  test('Blacknet utils numberOfLeadingZeros', () {
    expect(numberOfLeadingZeros(8), 28);
  });
  test('Blacknet utils encodeVarInt and decodeVarInt', () {
    int l = 1000;
    Uint8List u = encodeVarInt(l);
    expect(decodeVarInt(u), l);
  });

  test('Generate Mnemonic with bip39', () {
    expect(Blacknet.generateMnemonic().split(' ').length, 12);
  });
  test('Address with mnemonic', () {
    expect(Blacknet.address(testMnemonic), testAccount);
  });
  test('Signature with serialized', (){
    expect(Blacknet.signature(testMnemonic, testSerialized), testSignature);
  });
  test('Sign with message', (){
    expect(Blacknet.sign(testMnemonic, testMessage), testSign);
  });
  test('Verify with signature and message', (){
    expect(Blacknet.verify(testAccount, testSign, testMessage), true);
  });
  test('Decrypt', (){
    expect(Blacknet.decrypt(testMnemonic2, testAccount2, testEncrypted), testStr);
  });
  test('Hash', (){
    var expected = Uint8List.fromList([31, 255, 8, 136, 109, 125, 233, 59, 32, 233, 33, 120, 145, 163, 51, 197, 140, 103, 101, 3, 44, 139, 165, 237, 116, 221, 198, 169, 212, 170, 181, 85]);
    expect(Blacknet.hash(testMnemonic2), expected);
  });
  test('Encrypt And Decrypt', (){
    var encrypt = Blacknet.encrypt(testMnemonic2, testAccount, testStr);
    var decrypt = Blacknet.decrypt(testMnemonic, testAccount2, encrypt);
    expect(decrypt, testStr);
  });
  test('Serialize With Transfer', () async{
      FormData formData = new FormData.fromMap({
        "fee": (0.001 * 1e8).toInt(),
        "amount": (1 * 1e8).toInt(),
        "message": "xxxx",
        "to": testAccount2,
        "encrypted": 1,
        "from": testAccount2
      });
      var body = await serialize.transfer(formData);
      expect(body.code, 200);
  });
  test('Serialize With Lease', () async{
      FormData formData = new FormData.fromMap({
        "fee": (0.001 * 1e8).toInt(),
        "amount": (1 * 1e8).toInt(),
        "to": testAccount2,
        "from": testAccount2
      });
      var body = await serialize.lease(formData);
      expect(body.code, 200);
  });
  test('Serialize With CancelLease', () async{
      FormData formData = new FormData.fromMap({
        "fee": (0.001 * 1e8).toInt(),
        "amount": (1 * 1e8).toInt(),
        "to": testAccount2,
        "height": 1111111,
        "from": testAccount2
      });
      var body = await serialize.cancelLease(formData);
      expect(body.code, 200);
  });
  test('Serialize With WithdrawFromLease', () async{
      FormData formData = new FormData.fromMap({
        "fee": (0.001 * 1e8).toInt(),
        "withdraw": (1 * 1e8).toInt(),
        "amount": (1 * 1e8).toInt(),
        "to": testAccount2,
        "height": 1111111,
        "from": testAccount2
      });
      var body = await serialize.cancelLease(formData);
      expect(body.code, 200);
  });
  test('GetSeq', () async{
      var body = await serialize.getSeq(testAccount2);
      expect(body.code, 200);
  });
  test('Transfer', () async{
      var body = await blacknet.jsonrpc.transfer(testMnemonic2, testAccount2, testAccount2, 1);
      expect(body.code, 200);
  });
  test('Lease', () async{
      var body = await blacknet.jsonrpc.lease(testMnemonic2, testAccount2, testAccount2, 1);
      expect(body.body, "Transaction rejected: 100000000 less than minimal 100000000000");
  });
  test('CancelLease', () async{
      var body = await blacknet.jsonrpc.cancelLease(testMnemonic2, testAccount2, testAccount2, 1000, 1);
      expect(body.body, "Transaction rejected: Lease not found");
  });
  test('WithdrawFromLease', () async{
      var body = await blacknet.jsonrpc.withdrawFromLease(testMnemonic2, testAccount2, testAccount2, 1000, 1000, 1);
      expect(body.body, "Transaction rejected: Can not withdraw more than -99900000000");
  });
}
