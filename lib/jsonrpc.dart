import 'dart:convert';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:blacknet_lib/blacknet.dart';
import 'package:blacknet_lib/serialize.dart';
import 'package:blacknet_lib/request.dart';
// JSONRPC
class JSONRPC {
  String endpoint;
  Request request;
  Serialize serialize;
  JSONRPC({this.endpoint}){
    this.request = new Request();
    this.serialize = new Serialize(endpoint: this.endpoint);
  }

  double getFees(String message){
    var minTxFee = 0.001;
    if (message == null) {
        return minTxFee;
    }
    var normalSize = 184;
    var messageSize = utf8.encode(message).length;
    var total = normalSize + messageSize;
    
    return minTxFee * (1 + total / 1000);
  }

  Future<Body> transfer(String mnemonic, String from, String to, double amount,
      [double fee, String message, bool encrypted]) async {
        if (fee == null) {
          fee = getFees(message);
        }
    FormData formData = new FormData.fromMap({
      "fee": (fee * 1e8).toInt(),
      "amount": (amount * 1e8).toInt(),
      "to": to,
      "from": from
    });
    if (encrypted != null && encrypted) {
      formData.fields.add(MapEntry("encrypted", "1"));
    }
    if (message != null) {
      formData.fields.add(MapEntry("message", message));
    }
    var serializedRes = await this.serialize.transfer(formData);
    if (serializedRes.code != 200) {
      return serializedRes;
    }
    var signature = Blacknet.signature(mnemonic, serializedRes.body);

    return this.request.get([
      this.endpoint+ "/api/v2/sendrawtransaction",
      signature
    ].join("/"));
  }

  Future<Body> lease(String mnemonic, String from, String to, double amount, [double fee]) async {
    if (fee == null) {
      fee = 0.001;
    }
    FormData formData = new FormData.fromMap({
      "fee": (fee * 1e8).toInt(),
      "amount": (amount * 1e8).toInt(),
      "to": to,
      "from": from
    });
    var serializedRes = await this.serialize.lease(formData);
    if (serializedRes.code != 200) {
      return serializedRes;
    }
    var signature = Blacknet.signature(mnemonic, serializedRes.body);

    return this.request.get([
      this.endpoint+ "/api/v2/sendrawtransaction",
      signature
    ].join("/"));
  }

  Future<Body> cancelLease(String mnemonic, String from, String to, int height, double amount, [double fee]) async {
    if (fee == null) {
      fee = 0.001;
    }
    FormData formData = new FormData.fromMap({
      "fee": (fee * 1e8).toInt(),
      "amount": (amount * 1e8).toInt(),
      "to": to,
      "height": height,
      "from": from
    });
    var serializedRes = await this.serialize.cancelLease(formData);
    if (serializedRes.code != 200) {
      return serializedRes;
    }
    var signature = Blacknet.signature(mnemonic, serializedRes.body);

    return this.request.get([
      this.endpoint+ "/api/v2/sendrawtransaction",
      signature
    ].join("/"));
  }

  Future<Body> withdrawFromLease(String mnemonic, String from, String to, int height, double withdraw, double amount, [double fee]) async {
    if (fee == null) {
      fee = 0.001;
    }
    FormData formData = new FormData.fromMap({
      "fee": (fee * 1e8).toInt(),
      "withdraw": (amount * 1e8).toInt(),
      "amount": (amount * 1e8).toInt(),
      "to": to,
      "height": height,
      "from": from
    });
    var serializedRes = await this.serialize.withdrawFromLease(formData);
    if (serializedRes.code != 200) {
      return serializedRes;
    }
    var signature = Blacknet.signature(mnemonic, serializedRes.body);

    return this.request.get([
      this.endpoint+ "/api/v2/sendrawtransaction",
      signature
    ].join("/"));
  }
  
  Future<Body> getSeq(String account) async {
    return this.request.get([
      this.endpoint+ "/api/v1/walletdb/getsequence",
      account
    ].join("/"));
  }
}