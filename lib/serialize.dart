import 'package:dio/dio.dart';
import 'package:blacknet_lib/request.dart';
import 'package:blacknet_lib/blacknet/message.dart';
import 'package:blacknet_lib/blacknet/transaction.dart';
import 'package:blacknet_lib/blacknet/hash.dart';
import 'package:blacknet_lib/blacknet/utils.dart';
// Serialize
class Serialize {
  String endpoint;
  Request request;
  Serialize({this.endpoint}){
    this.request = new Request();
  }

  Future<Body> transfer(FormData form) async {
    // form.fields
    Body body = await this.getSeq(form.fields.singleWhere((value) => value.key == "from").value);
    if (body.code != 200) {
      return body;
    }
    Message m = Message(Message.plain, form.fields.singleWhere((value) => value.key == "message").value);
    Transfer d = new Transfer(int.parse(form.fields.singleWhere((value) => value.key == "amount").value), form.fields.singleWhere((value) => value.key == "to").value, m);
    Transaction t = new Transaction(form.fields.singleWhere((value) => value.key == "from").value, int.parse(body.body), Hash.empty(), int.parse(form.fields.singleWhere((value) => value.key == "fee").value), 0, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> lease(FormData form) async {
    Body body = await this.getSeq(form.fields.singleWhere((value) => value.key == "from").value);
    if (body.code != 200) {
      return body;
    }
    Lease d = new Lease(int.parse(form.fields.singleWhere((value) => value.key == "amount").value), form.fields.singleWhere((value) => value.key == "to").value);
    Transaction t = new Transaction(form.fields.singleWhere((value) => value.key == "from").value, int.parse(body.body), Hash.empty(), int.parse(form.fields.singleWhere((value) => value.key == "fee").value), 2, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> cancelLease(FormData form) async {
    Body body = await this.getSeq(form.fields.singleWhere((value) => value.key == "from").value);
    if (body.code != 200) {
      return body;
    }
    CancelLease d = new CancelLease(int.parse(form.fields.singleWhere((value) => value.key == "amount").value), form.fields.singleWhere((value) => value.key == "to").value, int.parse(form.fields.singleWhere((value) => value.key == "height").value));
    Transaction t = new Transaction(form.fields.singleWhere((value) => value.key == "from").value, int.parse(body.body), Hash.empty(), int.parse(form.fields.singleWhere((value) => value.key == "fee").value), 3, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> withdrawFromLease(FormData form) async {
    Body body = await this.getSeq(form.fields.singleWhere((value) => value.key == "from").value);
    if (body.code != 200) {
      return body;
    }
    WithdrawFromLease d = new WithdrawFromLease(int.parse(form.fields.singleWhere((value) => value.key == "withdraw").value), int.parse(form.fields.singleWhere((value) => value.key == "amount").value), form.fields.singleWhere((value) => value.key == "to").value, int.parse(form.fields.singleWhere((value) => value.key == "height").value));
    Transaction t = new Transaction(form.fields.singleWhere((value) => value.key == "from").value, int.parse(body.body), Hash.empty(), int.parse(form.fields.singleWhere((value) => value.key == "fee").value), 11, d.serialize());
    return new Body(200, Utils.toHex(t.serialize()));
  }

  Future<Body> getSeq(String account) async {
    return this.request.get([
      this.endpoint+ "/api/v1/walletdb/getsequence",
      account
    ].join("/"));
  }
}